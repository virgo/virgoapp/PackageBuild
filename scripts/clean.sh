# SPDX-License-Identifier: GPL-2.0-or-later

package_path=$1
tag=$2
constituent=$3

build_path="${package_path}/${tag}/build"
constituent_path="${build_path}/${constituent}"

set -e

rm -f "${build_path}/${constituent}_extract_done"
rm -f "${build_path}/${constituent}_install_done"
rm -f "${build_path}/${constituent}.build.log"
rm -rf "${build_path}/${constituent}/"
