#!/bin/sh

# SPDX-License-Identifier: GPL-2.0-or-later

root=$1
tag=$2
python_package=$3

set -e

if [ -e "${root}/${tag}/bin/python3" ]
then
	exit 0
fi

if [ ! -f "${root}/sources/${python_package}.tar.gz" ]
then
	echo "${python_package}.tar.gz not found !"
	exit 1
fi

mkdir -p "${root}/${tag}/"
rm -rf "${root}/${tag}/${python_package}"
tar -xvzf "${root}/sources/${python_package}.tar.gz" -C "${root}/${tag}/"

if [ -f "${root}/sources/python.patch" ]
then
	patch -p1 -d "${root}/${tag}/${python_package}" < "${root}/sources/python.patch"
fi

cd "${root}/${tag}/${python_package}"
./configure --prefix="${root}/${tag}/"
make
make install

if [ ! -e "${root}/${tag}/bin/python" ]
then
	ln -s "${root}/${tag}/bin/python3" "${root}/${tag}/bin/python"
fi
