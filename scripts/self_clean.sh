# SPDX-License-Identifier: GPL-2.0-or-later

package_path=$1
tag=$2
constituent=$3

build_path="${package_path}/${tag}"

set -e

rm -rf "${build_path}-${constituent}/"
