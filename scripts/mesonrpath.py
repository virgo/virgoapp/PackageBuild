# SPDX-License-Identifier: GPL-2.0-or-later

import subprocess
import json
import os
import sys

def set_rpath (rpath):
    if not rpath:
        return

    print ("Set shared library and executable rpath to '{}':".format(rpath))
    try:
        output = subprocess.run(["meson", "introspect", "--targets", "--indent"], stdout=subprocess.PIPE, check=True)
        data = json.loads(output.stdout)
        for target in data:
            if target['installed']:
                if target['type'] in ['executable', 'shared library', 'shared module']:
                    for filename in target['install_filename']:
                        try:
                            subprocess.run(["patchelf", "--set-rpath", rpath, filename], check=True)
                            subprocess.run(["patchelf", "--shrink-rpath", filename], check=True)
                            print (filename)
                        except:
                            pass
                else:
                    try:
                        print("Ignore '{0}' with type '{1}'".format(target['name'], target['type']))
                    except:
                        pass

    except Exception as error:
        print(error)
        sys.exit(1)
