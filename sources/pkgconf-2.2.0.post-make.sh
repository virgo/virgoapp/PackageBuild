set -e

rm -f ${PB_PACKAGE_INSTALL_PREFIX}/bin/pkg-config
ln -s ${PB_PACKAGE_INSTALL_PREFIX}/bin/pkgconf ${PB_PACKAGE_INSTALL_PREFIX}/bin/pkg-config
