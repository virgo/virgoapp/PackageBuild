# PackageBuild

## Introduction

This package is an helper for the installation of external source packages using CMT. It takes care to untar the source archive, apply patches, configure the compilation then proceed to the compilation and the installation in the standard binary directory. A single CMT package using PackageBuild can contain several source packages. The compiler and linker options for each installed source packages can be determined using pkgconf (pkg-config compatible replacement), which is bundled in PackageBuild.

Starting from v1r0, PackageBuild can also be used as a mean to delegate the actual compilation of a CMT package to either CMake or meson, using a minimalist `requirements` file.

## Installation

PackageBuild is a standard CMT package. Once you have retrieved the sources from the version control system, the compilation is done with
the following commands:

```
cd PATH_TO_PACKAGE_SOURCE/vxrypz/cmt
cmt config
make
```
For ease of deployment on older OS, PackageBuild bundles recent versions of CMake, Python3, Scons, meson and ninja. Meson, Scons and ninja are built by the default target, CMake and Python3 by the extras target (`make extras`).

## External package compilation

Here's the typical CMT package tree for using PackageBuild:

```
/vxry/cmt/requirements
/vxry/sources/library01.tar.gz
/vxry/sources/library01-fix01.patch
/vxry/sources/library01-fix02.patch
/vxry/sources/library01.options
/vxry/sources/library02.tar.bz2
/vxry/sources/library02-fix01.patch
/vxry/sources/library02.pre-configure.sh
/vxry/sources/library02.post-make.sh
/vxry/sources/library02.make.options
```

The source archives may be compressed either using bzip2, gzip or xz.  They must follow standard installation steps, either based on autotools, CMake, meson or Scons. Options and patch files are optional.

### Compilation Configuration Options

The options file consist in one or several lines, containing the parameters for the compilation configure script. Each lines in options correspond to a configure and installation sequence.

Here is an example of an options file, which will result in two configuration/compilation/installation passes:

```
--enable-shared
--enable-shared --enable-float
```

### Patches

The patch files must be generated using diff, and will be applied using `patch -p1`. It's better to have one patch file per fix.

Here is an example:

```diff
diff --git a/glib/glib/gthread-posix.c b/glib/glib/gthread-posix.c
index 3f74aa4..5f60256 100644
--- a/glib/glib/gthread-posix.c
+++ b/glib/glib/gthread-posix.c
@@ -1171,7 +1171,7 @@ g_system_thread_exit (void)
 void
 g_system_thread_set_name (const gchar *name)
 {
-#ifdef HAVE_SYS_PRCTL_H
+#ifdef PR_SET_NAME
   prctl (PR_SET_NAME, name, 0, 0, 0, 0);
 #endif
 }
```

### Make Options

It is possible to pass options to the invocation of the make command, either at the CMT package level, using a `macro pb_make_options` statement in the requirements file, or in a foo.make.options file. The make.options file overrides the `macro` statement. An example of the use of the `macro` statement is:

```
macro pb_make_options "-j10"
```

### Additional Hooks

For non standard needs, two additional scripts may be used,
foo.pre-configure.sh and foo.post-make.sh, which are respectively executed
before ./configure and after make install.

### Ignoring Packages

While discouraged, it is possible to ignore the installation of a set of
packages and to use the system installed versions. A colon seperated list of
packages to ignore should be set in the `PB_IGNORE_PACKAGES` environment
variable. For example:

```sh
export PB_IGNORE_PACKAGES=fftw-3.3.*:root
```

### Example

This is a typical `requirements` file:

```
package ExternalLibraries

use PackageBuild v0r13

apply_pattern pb_paths

private

set pb_make_options	"-j10"

document pb_build	"library01"
document pb_build	"library02"
document pb_build	"library03"
```

The `pb_paths` pattern take care to prepend the CMT package directories to `pb_rpath`, `pb_pkg_config_path` and
`pb_cmake_prefix_path`.

- `${PACKAGEROOT}/${tag}/lib` is prepended to `pb_rpath`. This path is used for setting the library and executable RPATH.
- `${PACKAGEROOT}/${tag}/lib/pkgconfig` is prepended to `pb_pkg_config_path`. This path is used as a search path for the pkgconf tool.
- `${PACKAGEROOT}/${tag}` is prepended to `pb_cmake_prefix_path`. This path is used as a search path for the CMake find_package tool.

The `pb_build` document makes use of these paths for the compilation configuration.

At the end of the compilation process, the source tree is deleted in order to free some disk space. This deletion can be prevented by
setting `PB_NO_BUILD_CLEANUP` environment variable to any value. A copy of the config.log file is kept in
`'${tag}/build/${constituent}.build.log'` file.

A `requirements` file for an application using ExternalLibraries would be:

```
package MyWonderfulApp

use ExternalLibraries

private

apply_pattern pb_flags packages="library01 library03"

application mwa mwa.c mwatool.c
```

The `pb_flags` pattern uses pkgconf in order to set `{PACKAGE}_cflags`, `{PACKAGE}_cppflags` and `${PACKAGE}_linkopts`. The `packages` parameter takes a list of pkgconf dependencies. It is also possible to set `{PACKAGE}_cflags`, `{PACKAGE}_cppflags` or `${PACKAGE}_linkopts` separately using `pb_cflags`, `pb_cppflags` or `pb_linkopts` patterns.

## CMake or meson compilation delegation

It is possible to use PackageBuild to assign the actual compilation to either CMake or meson. An example of a minimal
`requirements` file is:

```
package Fr

manager "Benoît Mours <benoit.mours@iphc.cnrs.fr>"

use PackageBuild v0r20

apply_pattern pb_cmake execs="FrDump FrCopy FrCheck"
```

In this case, CMake being used as the main build system, a `CMakeLists.txt` file must be present at the root of the package.

The `execs` parameter is optional. When present, it will create links to the listed executables, suffixed with `.exe` and located in the
`${tag}` directory.

Here is an example of a package using meson:

```
package Frv

manager "Benoît Mours <benoit.mours@iphc.cnrs.fr>"

use Fr   v8r38
use FFTW v3r3p35

apply_pattern pb_meson
```

A `meson.build` file must be present at the root of the package.

`pb_cmake` and `pb_meson` patterns automatically apply the `pb_paths` pattern.

Of course the CMT `use` statements should be keep synchronized with what is needed by the main build system.

It is possible to give a set of configuration flags to the main build system, using either `pb_meson_options` or
`pb_cmake_options` CMT macros.

For example:

```
macro pb_meson_options "-Dbuildtype=debug -Dstrip=true"
```

The build directories used by CMake or meson are respectively `${tag}-cmake/` and `${tag}-meson/`. On 64 bit platform, the installation of
libraries is forced to `${tag}/lib/` instead of `${tag}/lib64/`, using respectively `CMAKE_INSTALL_LIBDIR` and `libdir` configuration
parameters.

The `RPATH` property of the built executables and libraries is automatically set to a list of paths pointing to the dependency
`${tag}/lib` directories.

### How to develop using CMT with compilation delegation

It is more or less the same as working directly with CMT:

```
cd cmt
cmt config
cmt make
```

The first call to `cmt make` will configure the meson/cmake build directory, and invoke `ninja install` from there. All subsequent calls
will only invoke `ninja install` (This call moves the binaries and generated files to CMT ${tag} directory) if the configuration flags are
unchanged.

By default, the build type is `debug-optimized` for meson or `RelWithDebInfo` for CMake (Please refer to CMake and meson
documentation for the allowed values). It can be forced to another type by setting the `PB_BUILD_TYPE` environment variable in the `make`
call:

```
make PB_BUILD_TYPE=release
```

Please note you have to set the environment variable at each `make` call, otherwise the build type will switch back to the default one.
Alternatively, `PB_BUILD_TYPE` can be set permanently to the desired value using `export` shell command (or `setenv` with tcsh).

Similarly, the `make` output can be made more verbose using the `VERBOSE` environment variable:

```
make VERBOSE=1
```

You can combine `VERBOSE` and `PB_BUILD_TYPE` in the same command.

You may want to work directly in the cmake/meson build directory. In order to have a direct access to the cmake/meson/ninja versions used by
PackageBuild, you can use the CMT setup script from the PackageBuild `cmt` directory`:

```
cd cmt
cmt config
cmt make
source $PACKAGEBUILDROOT/cmt/setup.sh # or setup.csh
cd ../${UNAME}-cmake # or -meson
ninja install
```

And then call `ninja` for a simple compilation, or `ninja install` for the compilation and installation in the CMT ${tag} directory. `ninja
-v` builds with verbose mode activated.
