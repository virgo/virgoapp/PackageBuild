---
title: CMT with Meson or CMake build delegation 
author: Emmanuel Pacaud <emmanuel.pacaud@lapp.in2p3.fr>
date: 2020-05-13
---

## Definitions

### Source directory

This is the location of the package sources. For Virgo, we are using a flat layout, where all packages are stored in a limited set of directories, listed in the `${CMTPATH}` environment variable. `/virgoApp` is the main reference directory, where the packages can be installed by anyone using PackageManagement.

### Build directory

This is where binaries and build artifacts are stored.

In CMT, most of the generated files are stored in the `${tag}` directory. The value is set from the `${UNAME}` environment variable, built from the OS and hardware informations (`Linux-x86_64-CL7` for the current baseline). Some other files are generated in the source tree, like the prototype header files (*.ph) created in the same directory as the corresponding source file.

CMake and Meson have also the same concept of build directory, but the exact location is let to the user at the initialisation of the build. A standard build directory location is `build` at the top level source directory.

### Installation prefix

While it is possible to install the result of the compilation in a common place with CMT, in Virgo we are using directly what is stored in the build directory. The interesting thing being we don't have to worry about conflicts between installed files, and we can keep a complete history of the compiled versions.

With CMake/Meson, the installation is usually done in a common area, the root of this area being set by specifying an installation prefix, `/usr/local` by default.

### Dependency

In CMT dependencies are defined using the `use` statement. In Virgo, most of the time we enforce the use of a completely specified version (no wildcard).

The compilation parameters (location of include files, cflags, cppflags, linkopts) are retrieved by CMT by parsing the `requirements` file of the project dependencies.

In order to be able to execute the applications from the CMT build directory, we usually have to set the rpath property of the libraries and executable by adding a `-Wl,rpath=${package_root}\${tag}` parameter to linkopts.

CMake and Meson don't use the project configuration files (equivalent to `requirements`), but instead relies on `pkg-config` or `cmake` files. Both build tools are able to use either one.

## Port of a CMT package to CMake/Meson delegation

All the magic happens in the PackageBuild CMT package, which should be the base dependency of all packages using CMake/Meson.

The base concepts of PackageBuild are:

* Runtime paths is forced into libraries and executables
* The actual build configuration is done by either Meson or CMake, and the compilation uses Ninja, a make equivalent.
* The Meson or CMake build should be self-consistent, which means it should be possible to build the package without CMT.
* Build dependencies are handled through pkg-config `*.pc` files. A project must generate one or several pkg-config files if it is intended as a dependency of another package.
* CMT version requirements should define the exact version to be used, while the version requirements in Meson/CMake should only set the minimum compatible version.

We will use a port of Frv to Meson as an example.

### `requirements` file

The `requirements` file of Frv is reduced to a few lines:

```
package Frv

use Fr   v8r39p1 
use FFTW v3r3p82

apply_pattern pb_meson
```

The `use` part is unchanged. The dependency tree is the following:

```
cmt show uses
# use Fr v8r39p1 
#   use PackageBuild v1r2 
# use FFTW v3r3p82
#   use PackageBuild v1r2
```

The interesting line is the use of the `pb_meson` pattern.

This pattern does the following:

* Prepend `${PackageRoot}/${tag}/lib/pkgconfig` to the pkgconfig directory list (stored in `pb_pkg_config_path` path)
* Prepend `${PackageRoot}/${tag}/` to the cmake directory list (stored in `pb_cmake_prefix_path` path)
* Prepend `${PackageRoot}/${tag}/` to the rpath directory list (stored int `pb_rpath` path)

* Call a script that:

  * Configures the build, using the previously defined directory lists.
  * Compiles the sources using ninja.
  * Install the built files in `${tag}`.

* Optionally create symbolic links to binaries installed in `${tag}/bin`, with a `.exe` suffix, for compatibility with the previous pure CMT package. A list of space separated applications should be given using `execs="... ..."` parameter. For example, in Fr, we have:

```
package Fr

use PackageBuild v1r2

apply_pattern pb_cmake execs="FrDump FrCopy FrCheck"
```

Which creates `${tag}/FrDump.exe`, `${tag}/FrCopy.exe` and `${tag}/FrCheck.exe` symbolic links.

In order to compile a CMT package with CMake/Meson compilation delegation, a regular `make` is needed.

PackageBuild uses a build directory named either '${tag}-meson` or '${tag}-cmake'.

A reset of the build is obtained with:

```
make clean
make binclean
```

The compilation can be made verbose with the usual command:

```
make VERBOSE=1
```

### Meson files

The actual build is ported to Meson in 2 `meson.build` files, located in the top level directory and in the `src` directory. The 2 files could be combined in one, but it is generally more convenient to have a `meson.build` in each source subdirectory.

There was an agreement about using the same lower case string for the project name and the `pkg-config` file name. If the package provide only one library, the library should also use the same name. It is preferable to use a string of 3 or more characters in order to avoid possible collision with other projects.

Here is the content of each file. `meson.build`:

```
project('frv', 'c', version:'4.34.2', default_options:['c_std=c99'])

pkgconfig = import('pkgconfig')

cc = meson.get_compiler('c')

libm = cc.find_library('m')
frame = dependency('framel', version:'>=8.38.1')
fftw = dependency('fftw3', version:'>=3.3.8')
fftwf = dependency('fftw3f')

frv_deps = [frame, fftw, fftwf]
frv_priv_deps = [libm]

subdir('src')
```

`src/meson.build`:

```
sources = [
  'FrvVersion.c',
  'FrvCopy.c',
  'FrvMath.c',
  'FrvFFT.c',
  'FrvTF.c',
  'FrvBuf.c',
  'FrvCh.c',
  'FrvFilter.c',
  'FrvLinFilt.c',
  'FrvSmartDecimate.c',
  'FrvFDFilter.c',
  'FrvCorr.c'
  ]

headers = [
  'FrvCopy.h',
  'FrvMath.h',
  'FrvFFT.h',
  'FrvTF.h',
  'FrvBuf.h',
  'FrvCh.h',
  'FrvFilter.h',
  'FrvLinFilt.h',
  'FrvSmartDecimate.h',
  'FrvFDFilter.h',
  'FrvCorr.h'
  ]

frv_library = library(meson.project_name(), sources, headers,
  dependencies: frv_deps + frv_priv_deps, version:meson.project_version(), 
  c_args:['-DFRV_PATH="' + meson.source_root() + '"',
          '-DFRV_VERSION="' + meson.project_version() + '"'],
  install:true)

pkgconfig.generate(frv_library, libraries: frv_deps, name: meson.project_name(), description:'Frame vector computation library')

foreach e: [ 'FrvMathTest', 'FrvCopyTest', 'FrvFilterTest', 'FrvTFTest' ]
  executable(e, e+'.c', link_with:frv_library, dependencies:frv_deps + frv_priv_deps, install:true)
endforeach
```

### CMake example

If the choosen build system is CMake, the `requirements` file is almost unchanged, `pb_meson` pattern is just replaced with the `pb_cmake` pattern.

```
package Frv

use Fr   v8r39p1 
use FFTW v3r3p82

apply_pattern pb_cmake
```

The `CMake` files are named `CMakeLists.txt` and are also placed in each source subdirectory. `CMakeLists.txt`:

```
project(
	frv
	LANGUAGES C
	VERSION 4.34.2
	DESCRIPTION "VIRGO frame vector processing library"
	HOMEPAGE_URL "http://www.virgo-gw.eu/"
)

include(GNUInstallDirs)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -std=c99")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DFRV_PATH=\\\"${CMAKE_CURRENT_SOURCE_DIR}\\\"")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DFRV_VERSION=\\\"${PROJECT_VERSION}\\\"")
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG}")
set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -g")

set(LIBFRV_VERSION_STRING "${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH}")

find_package(PkgConfig REQUIRED)

set(FRV_DEPENDENCIES "framel >= 8.38.1" "fftw3 >= 3.3.8" "fftw3f")
pkg_check_modules(FRVDEPS REQUIRED IMPORTED_TARGET ${FRV_DEPENDENCIES})

add_subdirectory(src)

set(CPACK_PACKAGE_MAJOR ${${PROJECT_NAME}_MAJOR_VERSION})
set(CPACK_PACKAGE_MINOR ${${PROJECT_NAME}_MINOR_VERSION})
set(CPACK_PACKAGE_PATCH ${${PROJECT_NAME}_PATCH_VERSION})
set(CPACK_PACKAGE_VERSION "${CPACK_PACKAGE_MAJOR}.${CPACK_PACKAGE_MINOR}.${CPACK_PACKAGE_PATCH}")

set(CPACK_SOURCE_GENERATOR TXZ)
set(CPACK_SOURCE_PACKAGE_FILE_NAME ${PROJECT_NAME}-${${PROJECT_NAME}_VERSION})
set(CPACK_SOURCE_IGNORE_FILES
    "\\\\.svn"
    "\\\\.git"
    "\\\\.swp"
    "Linux-[.]*"
    "/cmt/"
    "/mgr/"
    "meson.build"
)

include(CPack)
```

`src/CMakeLists.txt`:

```
set(LIBFRV_HEADERS
	Frv.h
	FrvCopy.h
	FrvMath.h
	FrvFFT.h
	FrvTF.h
	FrvBuf.h
	FrvCh.h
	FrvFilter.h
	FrvLinFilt.h
	FrvSmartDecimate.h
	FrvFDFilter.h
	FrvCorr.h
	)

add_library(${PROJECT_NAME} SHARED
	FrvVersion.c
	FrvCopy.c
	FrvMath.c
	FrvFFT.c
	FrvTF.c
	FrvBuf.c
	FrvCh.c
	FrvFilter.c
	FrvLinFilt.c
	FrvSmartDecimate.c
	FrvFDFilter.c
	FrvCorr.c
	)

set_target_properties(
	${PROJECT_NAME} PROPERTIES
	VERSION ${LIBFRV_VERSION_STRING}
	SOVERSION ${PROJECT_VERSION_MAJOR}
	PUBLIC_HEADER "${LIBFRV_HEADERS}"
	)

target_link_libraries(${PROJECT_NAME} PUBLIC m PkgConfig::FRVDEPS)

install(TARGETS ${PROJECT_NAME}
	RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
	LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
	PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}
	)

foreach (app FrvCopyTest FrvFilterTest FrvMathTest FrvTFTest)
	add_executable(${app} ${app}.c)
	target_link_libraries(${app} ${PROJECT_NAME})
endforeach(app)

foreach(lib ${FRV_DEPENDENCIES})
	set(DEPENDENCIES "${DEPENDENCIES} ${lib}")
endforeach(lib)
configure_file(
	"${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME}.pc.in"
	"${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.pc"
	@ONLY
	)
install(
	FILES ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.pc
	DESTINATION ${CMAKE_INSTALL_LIBDIR}/pkgconfig
	)

```

### Meson or CMake

Being older than Meson, CMake has a wider adoption, mostly in the C++ world. But Meson is already adopted by big projects like GTK+, Xorg, GStreamer or Systemd.

Compared to CMake, Meson has the following strong points:

* syntax is nicer, quotes are required around strings
* pkgconfig file generation is integrated, and does not require a template
* no boilerplate code needed for tarball generation
* sane defaults for application and binary installation
* good documentation

### CMT wrapper for external packages

PackageBuild was initially developed to reliably install external source packages bundled in a CMT package.

Since the work on CMake/Meson delegation, the way the pkg-config directory list is managed is changed. A `pb_paths` pattern is available to manage this directory list. For example:

```
package Bar

apply_pattern pb_paths

document pb_build "bar-1.2.3"
```

### Use of PackageBuild based package from a regular CMT package

A `pb_flags` pattern allows to set `${Package}_cflags` and `${Package}_linkopts` using the pkg_config files. Here is an example:

```
package Foo

use Frv
use Bar

apply_pattern pb_flags packages="frv bar"

application Foo foo.c
```

## Links

### Meson

https://mesonbuild.com/

### CMake

https://cmake.org/documentation/

### PackageBuild

`/virgoApp/PackageBuild/v1r2/doc/PackageBuild.html`


